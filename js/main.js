"use strict";

const input = document.getElementById("input");
const p = document.createElement("p");

input.addEventListener("focus",function inpFoc(){
    input.style.border = "solid 1px green";
    p.remove();
});

input.addEventListener("blur",function inpBlur(){
    if(input.value < 0 || input.value === ""){
        input.style.border = "solid 1px red";
        p.textContent = "Please enter correct price"
        input.after(p);
        input.value = "";
    }else{
        input.style.border = "solid 1px grey";

        const span = document.createElement("span");
        const btn = document.createElement("button"); 

        btn.textContent = "X";
        btn.style.cssText = "border:none;border-radius:20px;font-size: 5px;vertical-align: middle;padding: 1px 2px;margin-left:3px"
        span.textContent = `Текущая цена: ${input.value}$`;

        span.append(btn);
        input.before(span);
        input.value = "";

        btn.addEventListener("click",function delSpan(){
            span.remove();
            btn.remove();
        });
    }
});







